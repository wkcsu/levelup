package Servlet;

import Utilities.Game;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GetUsedGames extends HttpServlet {

    public static final String username = "root";
    public static final String password = "19891016wk";
    private static List<Game> games = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String db_name = request.getServletContext().getInitParameter("DBName");
        String url = "jdbc:mysql://localhost:3306/" + db_name;
        ResultSet rs = null;
        games = new ArrayList<>();
        Game g = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection myConn = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = null;
            String query = "SELECT * FROM product WHERE status = ?";
            ps = myConn.prepareStatement(query);
            ps.setString(1, "used");
            rs = ps.executeQuery();
            while (rs.next()){
                g = new Game();
                g.setId(rs.getInt("id"));
                g.setName(rs.getString("name"));
                g.setCategory(rs.getString("category"));
                g.setDescription(rs.getString("description"));
                g.setPrice(rs.getDouble("price"));
                g.setCopies(rs.getInt("copies"));
                g.setCompany(rs.getString("company"));
                g.setImg_url(rs.getString("img-url"));
                games.add(g);
            }
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            request.setAttribute("usedGames", games);
            RequestDispatcher rd = request.getRequestDispatcher("/usedgames.jsp");
            rd.forward(request, response);
        }
    }
}
