package Servlet;

import Utilities.PastOrder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SearchOrder extends HttpServlet {

    public static final String username = "root";
    public static final String password = "19891016wk";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String db_name = request.getServletContext().getInitParameter("DBName");
        String url = "jdbc:mysql://localhost:3306/" + db_name;
        String card_number = request.getParameter("card_number");
        ResultSet rs = null;
        ArrayList<PastOrder> pastOrders = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection myConn = DriverManager.getConnection(url, username, password);
            PreparedStatement ps;
            String query = "SELECT id, order_time, order_total FROM levelup.order WHERE card_number = ? ORDER BY order_time DESC";
            ps = myConn.prepareStatement(query);
            ps.setString(1, card_number);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap<String, Integer> product_detail = new HashMap<>();
                String order_id = rs.getString("id");
                PastOrder po = new PastOrder();
                po.setOrder_id(order_id);
                PreparedStatement ps2;
                String sql = "SELECT product_id, copies FROM levelup.order_detail WHERE order_id = ?";
                ps2 = myConn.prepareStatement(sql);
                ps2.setString(1, order_id);
                ResultSet rs2;
                rs2 = ps2.executeQuery();
                while (rs2.next()) {
                    int p_id = rs2.getInt("product_id");
                    int copies = rs2.getInt("copies");
                    PreparedStatement ps3;
                    String query2 = "SELECT name FROM levelup.product WHERE id = ?";
                    ps3 = myConn.prepareStatement(query2);
                    ps3.setInt(1, p_id);
                    ResultSet rs3 = ps3.executeQuery();
                    while (rs3.next()) {
                        String game_name = rs3.getString("name");
                        product_detail.put(game_name, copies);
                    }
                }
                po.setProducts(product_detail);
                Date date = rs.getDate("order_time");
                po.setPurchase_date(date);
                double order_total = rs.getDouble("order_total");
                po.setTotal_price(order_total);
                pastOrders.add(po);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            request.setAttribute("all_orders", pastOrders);
            request.getRequestDispatcher("/past_orders.jsp").forward(request, response);
        }
    }
}
