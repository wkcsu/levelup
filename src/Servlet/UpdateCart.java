package Servlet;

import Utilities.Cart;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UpdateCart extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String gameName = request.getParameter("gamename");
        int copies = Integer.parseInt(request.getParameter("copies"));
        HttpSession session = request.getSession();
        Cart c = (Cart) session.getAttribute("cart");
        c.update(gameName, copies);
        double price = c.totalPrice();
        session.setAttribute("cart", c);
        session.setAttribute("total", price);
        request.getRequestDispatcher("/cart.jsp").forward(request, response);
    }
}
