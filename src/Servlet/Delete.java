package Servlet;

import Utilities.Cart;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Delete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int gameId = Integer.parseInt(request.getParameter("gameid"));
        HttpSession session = request.getSession();
        Cart c = (Cart) session.getAttribute("cart");
        c.delete(gameId);
        double total = c.totalPrice();
        session.setAttribute("cart", c);
        session.setAttribute("total", total);
        request.getRequestDispatcher("/cart.jsp").forward(request, response);
    }
}
