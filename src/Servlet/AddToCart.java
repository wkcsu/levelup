package Servlet;

import Utilities.Cart;
import Utilities.Game;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

public class AddToCart extends HttpServlet {

    public static final String username = "root";
    public static final String password = "19891016wk";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String db_name = request.getServletContext().getInitParameter("DBName");
        String url = "jdbc:mysql://localhost:3306/" + db_name;
        ResultSet rs = null;
        int game_id = Integer.parseInt(request.getParameter("idtoadd"));
        Game g = new Game();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection myConn = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = null;
            String query = "SELECT * FROM product WHERE id = ?";
            ps = myConn.prepareStatement(query);
            ps.setInt(1, game_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                g.setId(rs.getInt("id"));
                g.setName(rs.getString("name"));
                g.setCategory(rs.getString("category"));
                g.setDescription(rs.getString("description"));
                g.setPrice(rs.getDouble("price"));
                g.setCopies(rs.getInt("copies"));
                g.setCompany(rs.getString("company"));
                g.setImg_url(rs.getString("img-url"));
                Date dbDate = rs.getDate("publish-date");
                g.setRelease_date(new java.util.Date(dbDate.getTime()));
            }
            HttpSession session = request.getSession();
            if (session.getAttribute("cart") != null) {
                Cart c = (Cart) session.getAttribute("cart");
                c.add(g);
                session.setAttribute("cart", c);
            } else {
                Cart c = new Cart();
                c.add(g);
                session.setAttribute("cart", c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            double total = cart.totalPrice();
            session.setAttribute("total", total);
            request.setAttribute("game", g);
            request.getRequestDispatcher("/cart_success.jsp").forward(request, response);
        }
    }
}
