package Servlet;

import Utilities.Cart;
import Utilities.Game;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.util.GregorianCalendar;
import java.util.Random;

public class CheckOut extends HttpServlet {

    public static final String username = "root";
    public static final String password = "19891016wk";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String db_name = request.getServletContext().getInitParameter("DBName");
        String url = "jdbc:mysql://localhost:3306/" + db_name;
        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String address = request.getParameter("address");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String zipCode = request.getParameter("zipcode");
        String cardNumber = request.getParameter("cardnumber");
        String cardName = request.getParameter("nameoncard");
        int cardMonth = Integer.parseInt(request.getParameter("expmon"));
        int cardYear = Integer.parseInt(request.getParameter("expyear"));
        String cardSecCode = request.getParameter("seccode");
        String orderId = genId();
        GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
        Timestamp current_time = new Timestamp(gc.getTimeInMillis());
        HttpSession session = request.getSession();
        double total = (double) session.getAttribute("total");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection myConn = DriverManager.getConnection(url, username, password);
            PreparedStatement ps;
            String query = "INSERT INTO levelup.order (card_number, card_name, card_sec, card_month, card_year, first_name, last_name, address, city, state, zipcode, id, order_time, order_total)"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = myConn.prepareStatement(query);
            ps.setString(1, cardNumber);
            ps.setString(2, cardName);
            ps.setString(3, cardSecCode);
            ps.setInt(4, cardMonth);
            ps.setInt(5, cardYear);
            ps.setString(6, firstName);
            ps.setString(7, lastName);
            ps.setString(8, address);
            ps.setString(9, city);
            ps.setString(10, state);
            ps.setString(11, zipCode);
            ps.setString(12, orderId);
            ps.setTimestamp(13, current_time);
            ps.setDouble(14, total);
            ps.executeUpdate();
            Cart cart = (Cart) session.getAttribute("cart");
            for (Game game : cart.getCart().keySet()) {
                int game_id = game.getId();
                //System.out.println(game_id);
                int copies = cart.getCart().get(game);
                PreparedStatement preparedStatement;
                String sql = "INSERT INTO levelup.order_detail (order_id, product_id, copies) VALUES (?,?,?)";
                preparedStatement = myConn.prepareStatement(sql);
                preparedStatement.setString(1, orderId);
                preparedStatement.setInt(2, game_id);
                preparedStatement.setInt(3, copies);
                preparedStatement.executeUpdate();
                String q = "UPDATE levelup.product SET copies = copies - ? WHERE id = ?";
                preparedStatement = myConn.prepareStatement(q);
                preparedStatement.setInt(1, copies);
                preparedStatement.setInt(2, game_id);
                preparedStatement.executeUpdate();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //HttpSession session = request.getSession();
            session.invalidate();
            request.getRequestDispatcher("/pay_success.jsp").forward(request, response);
            //System.out.println(current_time);
        }
    }

    private String genId() {
        String id = "";
        char[] chars = {'A','B','C','D','E','F',
                        'G','H','I','J','K','L',
                        'M','N','O','P','Q','R',
                        'S','T','U','V','W','X',
                        'Y','Z','0','1','2','3',
                        '4','5','6','7','8','9'};
        for (int i = 0; i < 13; i++) {
            Random r = new Random();
            int index = r.nextInt(36);
            id += chars[index];
        }
        return id;
    }
}
