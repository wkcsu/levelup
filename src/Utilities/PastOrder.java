package Utilities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class PastOrder implements Serializable{
    private String order_id;
    private HashMap<String, Integer> products;
    private Date purchase_date;
    private double total_price;

    public PastOrder() {
    }

    public PastOrder(String order_id, HashMap<String, Integer> products, Date purchase_date, double total_price) {
        this.order_id = order_id;
        this.products = products;
        this.purchase_date = purchase_date;
        this.total_price = total_price;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public HashMap<String, Integer> getProducts() {
        return products;
    }

    public void setProducts(HashMap<String, Integer> products) {
        this.products = products;
    }

    public Date getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Date purchase_date) {
        this.purchase_date = purchase_date;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }
}
