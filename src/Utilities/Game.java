package Utilities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Game implements Serializable{
    private int id;
    private String name;
    private String category;
    private String description;
    private Double price;
    private int copies;
    private String company;
    private String status;
    private String img_url;
    private Date release_date;

    public Game() {
        this.name = "";
        this.category = "";
        this.description = "";
        this.copies = 0;
        this.price = 0.0;
        this.company = "";
        this.status = "new";
        this.img_url = "";
        this.release_date = null;
    }

    public Game(String name, String category, String description, Double price, int copies, String company, String status, String img_url, Date date) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.price = price;
        this.copies = copies;
        this.company = company;
        this.status = status;
        this.img_url = img_url;
        this.release_date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getRelease_date() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(this.release_date);
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }
}
