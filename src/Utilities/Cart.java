package Utilities;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class Cart {
    private Map<Game, Integer> cart;

    public Cart() {
        this.cart = new HashMap<>();
    }

    public void add(Game g) {
        if (inCart(g)) {
            updateCart(g);
        } else {
            cart.put(g, 1);
        }
    }

    public void update(String gameName, int copies) {
        for (Game item : cart.keySet()) {
            if (item.getName().equals(gameName)) {
                cart.put(item, copies);
                return;
            }
        }
    }

    public void delete(int game_id) {
        for (Game item : cart.keySet()) {
            if (item.getId() == game_id) {
                cart.remove(item);
                return;
            }
        }
    }

    public double totalPrice() {
        double total = 0.0;
        for (Game item : cart.keySet()) {
            total += item.getPrice() * cart.get(item);
        }
        DecimalFormat df = new DecimalFormat("#####.##");
        total = Double.parseDouble(df.format(total));
        return total;
    }

    public Map<Game, Integer> getCart() {
        return this.cart;
    }

    private boolean inCart(Game g) {
        boolean result = false;
        for (Game item : cart.keySet()) {
            if (item.getName().equals(g.getName()) && item.getStatus().equals(g.getStatus())) {
                result = true;
            }
        }

        return result;
    }

    private void updateCart(Game g) {
        for (Game item : cart.keySet()) {
            if (item.getName().equals(g.getName()) && item.getStatus().equals(g.getStatus())) {
                cart.put(item, cart.get(item) + 1);
                return;
            }
        }
    }
}
