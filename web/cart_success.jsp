<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/17/2017
  Time: 10:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp"/>
<title>${initParam.AppName} | Add to Cart</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item">
                    <a href="cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="card bg-white my-5 text-center">
        <div class="card-header text-dark">
            <h4 class="card-title mb-2">Item Add to Cart Successful!</h4>
        </div>
        <div class="card-body text-dark">
            <p class="card-text mb-3"><strong>${game.name}</strong> is in your shopping cart now.</p>
        </div>
        <div class="card-footer">
            <a href="/" class="btn btn-warning my-2">Continue Shopping</a>
            <a href="/cart.jsp" class="btn btn-success my-2">View Cart</a>
        </div>
    </div>
</div>
<c:import url="include/jsp/footer.jsp" />
