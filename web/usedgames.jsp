<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/17/2017
  Time: 6:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp" />
<title>${initParam.AppName} | Used Games</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item active">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item">
                    <a href="cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <ol class="breadcrumb bg-transparent mt-3">
        <li class="breadcrumb-item"><a href="index.jsp" class="text-info">Home</a></li>
        <li class="breadcrumb-item text-white active">Used Games</li>
    </ol>
    <c:forEach var="game" items="${usedGames}">
        <div class="row mb-3">
            <div class="col-md-2">
                <img src="include/img/thumbnails/${game.img_url}" alt="" class="img-fluid m-2">
            </div>
            <div class="col-md-7">
                <h3><a href="/details?game_name=${game.name}" class="text-white">${game.name}</a></h3>
                <p class="text-muted">by ${game.company}</p>
                <p class="text-light">Category: ${game.category}</p>
            </div>
            <div class="col-md-3">
                <h1 class="text-white mb-md-4">$${game.price}</h1>
                <c:choose>
                    <c:when test="${game.copies > 0}">
                        <form action="cart" method="post">
                            <input type="hidden" name="idtoadd" value="${game.id}">
                            <button type="submit" class="btn btn-success">Add to Cart</button>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <h4 class="text-danger">Sold Out</h4>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <hr>
    </c:forEach>
</div>

<%--<table border="2px" align="center">--%>
<%--<th>id</th>--%>
<%--<th>Name</th>--%>
<%--<th>Category</th>--%>
<%--<th>Price</th>--%>
<%--<c:forEach var="game" items="${newGames}">--%>
<%--<tr>--%>
<%--<td>${game.id}</td>--%>
<%--<td>${game.name}</td>--%>
<%--<td>${game.category}</td>--%>
<%--<td>${game.price}</td>--%>
<%--</tr>--%>
<%--</c:forEach>--%>
<%--</table>--%>
<c:import url="include/jsp/footer.jsp" />
