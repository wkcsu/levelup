<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/16/2017
  Time: 11:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp"/>
<title>${initParam.AppName} | Details</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item">
                    <a href="cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mb-5">
    <div class="card bg-light my-5">
        <div class="card-header">
            <ol class="breadcrumb bg-transparent" style="margin-bottom: -0.5em">
                <li class="breadcrumb-item"><a href="index.jsp" class="text-primary">Home</a></li>
                <li class="breadcrumb-item"><a href="/newgames" class="text-primary">New Games</a></li>
                <li class="breadcrumb-item active text-dark">Details</li>
            </ol>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <img src="include/img/regular/${game.img_url}" alt="" class="img-fluid m-2">
                </div>
                <div class="col-md-6">
                    <h2 class="text-dark">${game.name}</h2>
                    <p class="text-secondary">by ${game.company}</p>
                    <p class="text-muted"><small>Category: ${game.category}</small></p>
                    <p class="text-muted"><small>Release Date: ${game.release_date}</small></p>
                    <c:choose>
                        <c:when test="${game.copies > 0}">
                            <p class="text-muted"><small>Status: <span class="text-success">Available</span></small></p>
                        </c:when>
                        <c:otherwise>
                            <p class="text-muted"><small>Status: <span class="text-danger">Sold out</span></small></p>
                        </c:otherwise>
                    </c:choose>
                    <p class="text-muted">${game.description}</p>
                </div>
                <div class="col-md-3">
                    <h3 class="text-dark m-3"><strong>$</strong>${game.price}</h3>
                    <c:choose>
                        <c:when test="${game.copies > 0}">
                            <form action="/cart" method="post">
                                <input type="hidden" name="idtoadd" value="${game.id}">
                                <button type="submit" class="btn btn-primary m-3">Add to Cart</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <button class="btn disabled m-3" disabled>Sold Out</button>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>

</div>

<c:import url="include/jsp/footer.jsp" />