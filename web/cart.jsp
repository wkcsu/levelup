<%@ page import="java.util.HashMap" %>
<%@ page import="Utilities.Cart" %><%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/17/2017
  Time: 11:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp"/>
<title>${initParam.AppName} | Shopping Cart</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item active">
                    <a href="/cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <%
        if (session.getAttribute("cart") != null) {
            Cart cart = (Cart) session.getAttribute("cart");
            HashMap myCart = (HashMap) cart.getCart();
            if (myCart.size() == 0) {
                request.setAttribute("myCart", "empty");
            } else {
                request.setAttribute("myCart", myCart);
            }
        } else {
            request.setAttribute("myCart", "empty");
        }
    %>
    <ol class="breadcrumb bg-transparent mt-3">
        <li class="breadcrumb-item"><a href="index.jsp" class="text-info">Home</a></li>
        <li class="breadcrumb-item text-white active">Shopping Cart</li>
    </ol>
    <c:choose>
        <c:when test="${myCart == 'empty'}">
            <h3 class="text-light m-5">Your shopping cart is empty!</h3>
            <div>
                <a href="index.jsp" class="btn btn-warning my-3 mx-5">Continue Shopping</a>
            </div>
        </c:when>
        <c:otherwise>
            <div class="card">
                <div class="card-header">
                    Shopping Cart
                </div>
                <div class="card-body">
                    <c:forEach items="${myCart}" var="game">
                        <div class="row mt-4">
                            <div class="col-md-9">
                                <form action="/updateCart" method="post" class="form-inline">
                                    <input type="text" class="form-control mr-2" value="${game.key.name}" readonly name="gamename">
                                    <input type="number" min="1" max="${game.key.copies < 5 ? game.key.copies:5}" class="form-control mr-2" value="${game.value}" name="copies">
                                    <input type="text" class="form-control mr-5" value="$${game.key.price * game.value}" readonly name="totalprice">
                                    <button type="submit" class="btn-sm btn-info ml-5 form-control">Update</button>
                                        <%--<button type="submit" class="btn-sm btn-danger mr-4">Delete</button>--%>
                                </form>
                            </div>
                            <div class="col-md-3">
                                <form action="/delete" method="post" class="form-inline">
                                    <input type="hidden" name="gameid" value="${game.key.id}">
                                    <button type="submit" class="btn-sm btn-danger form-control mt-1 mr-5">Delete</button>
                                </form>
                            </div>
                        </div>
                    </c:forEach>
                    <%
                        double total = (double) session.getAttribute("total");
                    %>
                    <div>Total Price: $ <%=total%></div>
                </div>
                <div class="card-footer">
                    <a href="/checkout.jsp" class="btn btn-warning float-right">GO TO CHECKOUT</a>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<c:import url="include/jsp/footer.jsp"/>
