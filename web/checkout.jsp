<%@ page import="Utilities.Cart" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/21/2017
  Time: 6:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp"/>
<title>${initParam.AppName} | Check Out</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item active">
                    <a href="cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <ol class="breadcrumb bg-transparent mt-3">
        <li class="breadcrumb-item"><a href="index.jsp" class="text-info">Home</a></li>
        <li class="breadcrumb-item"><a href="/cart.jsp" class="text-info">Shopping Cart</a></li>
        <li class="breadcrumb-item active text-white">Check Out</li>
    </ol>
    <div class="row my-3">
        <div class="col-md-8 bg-light text-dark py-2">
            <form action="/checkout" method="post">
                <fieldset>
                    <legend>User Information</legend>
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" class="form-control form-control-sm" placeholder="First Name"
                               name="firstname" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" id="last_name" class="form-control form-control-sm" placeholder="Last Name"
                               name="lastname" required>
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" id="address" class="form-control form-control-sm" placeholder="Address"
                               name="address" required>
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" id="city" class="form-control form-control-sm" placeholder="City"
                               name="city" required>
                    </div>
                    <div class="form-group">
                        <label for="state">State</label>
                        <input type="text" id="state" class="form-control form-control-sm" placeholder="State"
                               name="state" required>
                    </div>
                    <div class="form-group">
                        <label for="zipcode">Zip Code</label>
                        <input type="text" id="zipcode" class="form-control form-control-sm" placeholder="Zip Code"
                               name="zipcode" required>
                    </div>
                    <div class="form-group">
                        <label for="cardnum">Card Number</label>
                        <input type="text" id="cardnum" class="form-control form-control-sm" placeholder="Card Number"
                               name="cardnumber" required>
                    </div>
                    <div class="form-group">
                        <label for="name_on_card">Name on Card</label>
                        <input type="text" id="name_on_card" class="form-control form-control-sm"
                               placeholder="Name on Card" name="nameoncard" required>
                    </div>
                    <div class="form-row">
                        <label for="expmonth">Expires</label>
                        <%
                            int[] months = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
                            int[] years = {2017, 2018, 2019, 2020, 2021, 2022,
                                    2023, 2024, 2025, 2026, 2027, 2028, 2029,
                                    2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037};
                        %>
                        <div class="col">
                            <select id="expmonth" class="form-control form-control-sm" name="expmon" required>
                                <c:forEach items="<%=months%>" var="month">
                                    <option>${month}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col">
                            <select id="expyear" class="form-control form-control-sm" name="expyear" required>
                                <c:forEach items="<%=years%>" var="year">
                                    <option>${year}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="seccode">Security Code</label>
                        <input type="number" name="seccode" id="seccode" class="form-control form-control-sm" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Place Order</button>
                </fieldset>
            </form>
        </div>
        <div class="col-md-4 bg-white text-warning py-2">
            <%
                if (session.getAttribute("cart") != null) {
                    Cart cart = (Cart) session.getAttribute("cart");
                    HashMap myCart = (HashMap) cart.getCart();
                    if (myCart.size() == 0) {
                        request.setAttribute("myCart", "empty");
                    } else {
                        request.setAttribute("myCart", myCart);
                    }
                } else {
                    request.setAttribute("myCart", "empty");
                }
            %>
            <c:choose>
                <c:when test="${myCart == 'empty'}">
                    <h3 class="text-light m-5">Your shopping cart is empty!</h3>
                    <div>
                        <a href="index.jsp" class="btn btn-warning my-3 mx-5">Continue Shopping</a>
                    </div>
                </c:when>
                <c:otherwise>
                    <%
                        double total = (double) session.getAttribute("total");
                    %>
                    <h3>Order Details</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Copies</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="game" items="${myCart}">
                                <tr>
                                    <td class="small">${game.key.name}</td>
                                    <td class="small">${game.value}</td>
                                    <td class="small">$${game.key.price * game.value}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <th>Total Price</th>
                                <td></td>
                                <td>$ <%=total%></td>
                            </tr>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<c:import url="include/jsp/footer.jsp"/>
