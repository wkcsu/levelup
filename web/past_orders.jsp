<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/22/2017
  Time: 9:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp" />
<title>${initParam.AppName} | Find Order</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item">
                    <a href="/cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item active">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <ol class="breadcrumb bg-transparent mt-3">
        <li class="breadcrumb-item"><a href="index.jsp" class="text-info">Home</a></li>
        <li class="breadcrumb-item text-white active">Order History</li>
    </ol>
    <c:choose>
        <c:when test="${empty all_orders}">
            <div class="card my-3">
                <div class="card-body">
                    <h4 class="text-warning">No order history found!</h4>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <c:forEach items="${all_orders}" var="order">
                <div class="card my-3">
                    <div class="card-header">
                        <h5>Order #: ${order.order_id}</h5>
                    </div>
                    <div class="card-body">
                        <p class="text-muted">${order.purchase_date}</p>
                        <table class="table table-striped">
                            <c:forEach var="entry" items="${order.products}">
                                <tr>
                                    <td>${entry.key}</td>
                                    <td>${entry.value}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <th>Total Price: </th>
                                <td>$${order.total_price}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</div>

<c:import url="include/jsp/footer.jsp" />
