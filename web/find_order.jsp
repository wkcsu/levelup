<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/22/2017
  Time: 8:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp" />
<title>${initParam.AppName} | Find Order</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item">
                    <a href="/cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item active">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <ol class="breadcrumb bg-transparent mt-3">
        <li class="breadcrumb-item"><a href="index.jsp" class="text-info">Home</a></li>
        <li class="breadcrumb-item text-white active">Find Orders</li>
    </ol>
    <form action="/search_orders" method="post" class="my-3">
        <div class="form-group text-light">
            <label for="card_number">Please enter your credit number:</label>
            <input type="text" class="form-control" id="card_number" name="card_number" required>
        </div>
        <button type="submit" class="btn btn-success">Search</button>
    </form>
</div>
<c:import url="include/jsp/footer.jsp" />
