<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/14/2017
  Time: 10:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@page import="java.util.GregorianCalendar, java.util.Calendar" %>
<%
    GregorianCalendar currentDate = new GregorianCalendar();
    int currentYear = currentDate.get(Calendar.YEAR);
%>
<!-- FOOTER -->
<footer id="main-footer" class="text-center p-4 bg-dark">
    <div class="container">
        <div class="row">
            <div class="col">
                <p>Copyright <%= currentYear %> &copy; Level Up</p>
            </div>
        </div>
    </div>
</footer>

<script src="include/js/jquery.min.js"></script>
<script src="include/js/popper.min.js"></script>
<script src="include/js/bootstrap.min.js"></script>
<script src="include/js/main.js"></script>
</body>
</html>

