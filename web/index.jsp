<%--
  Created by IntelliJ IDEA.
  User: Ge Wang
  Date: 11/5/2017
  Time: 8:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="include/jsp/header.jsp"/>
<%

%>
<title>${initParam.AppName} | Home</title>
</head>
<body>
<!-- NVABAR -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="index.jsp" class="navbar-brand">LEVELUP</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="index.jsp" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/newgames" class="nav-link">New Games</a>
                </li>
                <li class="nav-item">
                    <a href="/usedgames" class="nav-link">Used Games</a>
                </li>
                <li class="nav-item">
                    <a href="/cart.jsp" class="nav-link">Shopping Cart</a>
                </li>
                <li class="nav-item">
                    <a href="/find_order.jsp" class="nav-link">Orders</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <!-- SHOWCASE SLIDER -->
    <section id="showcase">
        <div id="myCarousel" class="carousel slide mb-5" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="row carousel-content">
                        <div class="col-md-8">
                            <img class="d-block img-fluid" src="include/img/codww2.jpg" alt="First Slide">
                        </div>
                        <div class="col-md-4 header-game-detail">
                            <h3 class="header-game-name">Call of Duty: WWII</h3>
                            <p class="game-description">Call of Duty: WWII returns to its roots with a breathtaking
                                experience that redefines World War II for a new gaming generation.</p>
                            <a href="/details?game_name=Call of Duty WWII" class="btn btn-danger header-btn">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row carousel-content">
                        <div class="col-md-8">
                            <img class="d-block img-fluid" src="include/img/aco.jpg" alt="Second Slide">
                        </div>
                        <div class="col-md-4 header-game-detail">
                            <h3 class="header-game-name">Assassin's Creed: Origins</h3>
                            <p class="game-description">ASSASSIN'S CREED: ORIGINS IS A NEW BEGINNING Ancient Egypt, a
                                land of majesty and intrigue, is disappearing in a ruthless fight for power.</p>
                            <a href="/details?game_name=Assassin's Creed: Origins" class="btn btn-primary header-btn">Buy
                                Now</a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row carousel-content">
                        <div class="col-md-8">
                            <img class="d-block img-fluid" src="include/img/wfst2.jpg" alt="Third Slide">
                        </div>
                        <div class="col-md-4 header-game-detail">
                            <h3 class="header-game-name">Wolfentein II: The New Colossus</h3>
                            <p class="game-description">You are BJ Blazkowicz, aka "Terror-Billy", member of the
                                Resistance, scourge of the Nazi empire, and humanity's last hope for liberty.</p>
                            <a href="/details?game_name=Wolfenstein II: The New Colossus"
                               class="btn btn-success header-btn">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#myCarousel" class="carousel-control-prev" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a href="#myCarousel" class="carousel-control-next" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </section>
    <section id="featured-items" class="py-5 text-center bg-secondary text-white">
        <h3>Featured Items</h3>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <img src="include/img/regular/codww2.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=Call of Duty WWII" class="text-light">Call of Duty WWII</a>
                </div>
                <p class="mt-3">$59.99</p>
            </div>
            <div class="col-md-4">
                <img src="include/img/regular/destiny2.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=Destiny 2" class="text-light">Destiny 2</a>
                </div>
                <p class="mt-3">$59.99</p>
            </div>
            <div class="col-md-4">
                <img src="include/img/regular/theevilwithin2.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=The Evil Within 2" class="text-light">The Evil Within 2</a>
                </div>
                <p class="mt-3">$59.99</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="include/img/regular/pubg_thumbnail.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=Playerunknown's Battlegrounds" class="text-light">Playerunknown's
                        Battlegrounds</a>
                </div>
                <p class="mt-3">$29.99</p>
            </div>
            <div class="col-md-4">
                <img src="include/img/regular/wolfenstein2.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=Wolfenstein II: The New Colossus" class="text-light">Wolfenstein II: The
                        New Colossus</a>
                </div>
                <p class="mt-3">$59.99</p>
            </div>
            <div class="col-md-4">
                <img src="include/img/regular/dishonored2.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=Dishonored 2" class="text-light">Dishonored 2</a>
                </div>
                <p class="mt-3">$39.99</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="include/img/regular/witcher3_featured.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=The Witcher III: Wild Hunt Complete Edition" class="text-light">The
                        Witcher 3: Wild Hunt Complete Edition</a>
                </div>
                <p class="mt-3">$29.99</p>
            </div>
            <div class="col-md-4">
                <img src="include/img/regular/xcom2.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=XCOM 2" class="text-light">XCOM 2</a>
                </div>
                <p class="mt-3">$59.99</p>
            </div>
            <div class="col-md-4">
                <img src="include/img/regular/wow_featured.jpg" alt="" class="img-fluid mb-2">
                <div class="mt-2">
                    <a href="/details?game_name=World of Warcraft: Legion" class="text-light">World of Warcraft:
                        Legion</a>
                </div>
                <p class="mt-3">$49.99</p>
            </div>
        </div>
    </section>
</div>
<c:import url="include/jsp/footer.jsp"/>
